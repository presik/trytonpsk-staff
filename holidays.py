# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
#from datetime import datetime
from trytond.model import ModelView, ModelSQL, fields


class Holidays(ModelSQL, ModelView):
    "Holidays"
    __name__ = "staff.holidays"
    holiday = fields.Date('Holiday', required=True)

    @classmethod
    def __setup__(cls):
        super(Holidays, cls).__setup__()
        cls._order.insert(0, ('holiday', 'ASC'))
