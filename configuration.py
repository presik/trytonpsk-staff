# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSingleton, ModelSQL, ModelView, fields
from trytond.modules.company.model import CompanyMultiValueMixin, CompanyValueMixin
from trytond.pool import Pool
from trytond.pyson import Eval, Id


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class Configuration(
    ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    "Staff Configuration"
    __name__ = 'staff.configuration'
    staff_contract_sequence = fields.Many2One('ir.sequence', 'Contract Sequence',
        required=True, domain=[('sequence_type', '=',
                                Id('staff', 'sequence_type_staff_contract'))])

    default_staff_contract_sequence = default_func('staff_contract_sequence')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'staff_contract_sequence':
            return pool.get('staff.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)


class StaffConfigurationSequence(ModelSQL, CompanyValueMixin):
    "Staff Configuration Sequence"
    __name__ = 'staff.configuration.sequence'
    staff_contract_sequence = fields.Many2One(
        'ir.sequence', "Staff Contract Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=', Id('staff', 'sequence_type_staff_contract')),
            ],
        depends=['company'])

    @classmethod
    def default_staff_contract_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('staff', 'sequence_staff_contract')
        except KeyError:
            return None
