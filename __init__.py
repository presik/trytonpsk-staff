# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import position
from . import employee
from . import holidays
from . import contract
from . import configuration


def register():
    Pool.register(
        holidays.Holidays,
        position.Position,
        employee.Employee,
        contract.StaffContract,
        configuration.Configuration,
        configuration.StaffConfigurationSequence,
        module='staff', type_='model')
