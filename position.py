# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Position(ModelSQL, ModelView):
    'Position'
    __name__ = 'staff.position'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    extras = fields.Boolean('Extras')

    @classmethod
    def __setup__(cls):
        super(Position, cls).__setup__()
